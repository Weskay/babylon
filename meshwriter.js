// You have to create a function called createScene. This function must return a BABYLON.Scene object

$.ajaxSetup({cache:true});
var typeURL     = "https://sanfrancisco.ca.illuminated.city/js/meshwriter.js";

var createScene = function() {
	var scene   = new BABYLON.Scene(engine);
    var scale   = 0.1, MeshWriter, text1, text2, text3, text4;
	var camera  = new BABYLON.ArcRotateCamera("Camera", -Math.PI/2, Math.PI/4, 25, BABYLON.Vector3.Zero(), scene);
	camera.attachControl(canvas, true);

    var light = new BABYLON.HemisphericLight("light1", new BABYLON.Vector3(0, 10, 0), scene);
    light.intensity = 0.5;

    // These lines load the module, if not already loaded, and then call write
    if ( typeof TYPE === "undefined" ) {
        jQuery.getScript(typeURL).then(write)
    } else {
        write()
    }
	return scene;

    function write () {
        Writer = BABYLON.MeshWriter(scene, {scale:scale});
        text1  = new Writer( 
                        "ABCDEFGHIJKLMNOPQRSTUVWXYZ",
                        {
                            "anchor": "center",
                            "letter-height": 50,
                            "font-family":"Jura",
                            "color": "#1C3870",
                            "position": {
                                "z": 20
                            }
                        }
                    );
        text2  = new Writer( 
                        "aábcdeéfghiíjklmnñoöôópqrstuvwxyz",
                        {
                            "anchor": "center",
                            "letter-height": 50,
                            "color": "#70381C",
                            "position": {
                                "z": -25
                            }
                        }
                    );
        text3  = new Writer(
                        "Z    a",
                        {
                            "anchor": "left",            // The default value; this line has no effect
                            "font-family":"Web-dings",
                            "letter-height": 20,
                            "color": "#70701C",
                            "position": {
                                "y": 40,
                                "x":  5
                            }
                        }
                    );
        text4  = new Writer(
                        "Z   a",
                        {
                            "anchor": "right",
                            "font-family":"Web-dings",
                            "letter-height": 20,
                            "color": "#70701C",
                            "position": {
                                "y": 40,
                                "x": -5
                            }
                        }
                    );

        // ~ ~ ~ ~ ~ ~ ~ ~ ~
        // text1, text2, etc. are MeshWriter instances
        // They have methods to permit retrieval and manipulation of the
        // sps, mesh and material they created
        //  . . . as you see below
        // ~ ~ ~ ~ ~ ~ ~ ~ ~
        setTimeout(half,10000);
        setTimeout(()=>text2.setColor("#1C5030"),15000);
        setTimeout(full,20000);
        setTimeout(half,30000);
        setTimeout(full,40000);
    }
    function half () { text3.setAlpha(0.5) ; text4.setAlpha(0.5) } ;
    function full () { text3.setAlpha(1.0) ; text4.setAlpha(1.0) } ;
};